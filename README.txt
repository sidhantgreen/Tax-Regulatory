A collection of annotated legal documents such as case laws etc. for ease of reference.

Subject to the rights reserved by the relevant authorities i.e. courts etc., this information is released in public domain with no rights reserved.
